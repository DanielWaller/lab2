package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Date;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		System.out.println("Start application new");
		System.out.println("Hello, I'm from develop_2 branch!");
		System.out.println("This is second message!");
		System.out.println(new Date());
		System.out.println("Ale duzo zmian (new_feature)");
		SpringApplication.run(DemoApplication.class, args);
	}

}
